from flask import Flask, jsonify, request
from flask_cors import cross_origin
import requests
import os
import dotenv

from interactive_conditional_samples import interact_model
app = Flask(__name__)

dotenv.load_dotenv()


@app.route('/detect', methods=['POST'])
@cross_origin(allow_headers=['Content-Type', 'Access-Control-Allow-Origin'])
def detect():
    api_key = os.environ.get('API_KEY')
    text = request.json['text']
    params = {'key': api_key, 'text': text, 'hint': 'en,fa'}
    url = 'https://translate.yandex.net/api/v1.5/tr.json/detect'
    res = requests.post(url=url, data=params)
    return jsonify(res.text)


@app.route('/translate', methods=['POST'])
@cross_origin(allow_headers=['Content-Type', 'Access-Control-Allow-Origin'])
def translate():
    api_key = os.environ.get('API_KEY')
    sl = request.json['sl']
    tl = request.json['tl']
    lang = sl + '-' + tl
    text = request.json['text']
    params = {'key': api_key, 'lang': lang, 'text': text}
    url = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
    res = requests.post(url=url, params=params)
    return jsonify(res.text)


@app.route('/sample', methods=['POST'])
@cross_origin(allow_headers=['Content-Type', 'Access-Control-Allow-Origin'])
def create_sample():
    text = request.json['text']
    print('input text:', text, sep=' ')
    samples = interact_model(input_text=text)
    print('samples:', samples, sep=' ')
    # samples = text + ' ' + text
    res = {
        'input': text,
        'samples': samples[0]
    }
    return jsonify(res)


if __name__ == '__main__':
    app.run()
